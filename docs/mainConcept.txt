Death Mask

Cyber Fantasy Adventure in endless Techno-Maze

Concept-Document

Credits

Ilia Prokhorov (Demens Deum) - main concept
Overview

Player is controlling main character named Revil. He comes to legendary Great Citadel to find Death Mask artifact.
Gameplay

Gameplay style - roguelike rpg in cyber fantasy setting.
Revil is trying to survive in Great Citadel, killing enemies, collecting items, eating, healing.
There is other adventures inside Techno-Maze. Their goal is same - to find Death Mask artifact.
Player can lose if will be not fast enough - other adventurer will gain control of Death Mask.
Death Mask

Artifact that allows owner to achieve eternal life. Mask itself requires energy from others souls, and always need more and more energy, that's why Techno-maze becomes bigger and bigger.
Art Style

Inspirations - Blame! Biomega manga, but with little dark places.
Characteristics

Let’s keep it simple:
Health - goes from 10 to zero, after zero - death
Attack - changes by weapon
Armor - changes by armor item
Hunger - goes from zero to 10, after 10 - decreasing health
Weapons

Handgun
Laser Handgun
Plasm Rifle
Laser Impactor (Sci-Fi Shotgun)
Grenade
EMP-Grenade
Insta-Gun (Railgun)
Destructor (Bazooka)
Armor
Citizen Vest
Tactical Vest
Police Armor
Combatant Armor
Daemon Armor
Friends & Enemies

Inside techno-maze Revil can find ordinary people that stuck inside, or live here from birth, adventurers, sellers.
Locals - has been born here inside biolabs, to feed Death Mask artifact power, but escaped by system error or sabotage. They need to eat, or healing, or can give something to eat, heal.
Adventurers - those who seek Death Mask artifact. Adventurers can be friendly if they are interested in it. Otherwise they just trying to kill Revil.
Sellers - people who sell and collect staff to/from adventurers, locals.
Daemons - creatures of techno-maze, that can control every aspect, change structure of maze if they want, or if there is order from the Core.
Crawlers - living creatures that has been captured by Techno-Maze. They are in different states, some of them behave like zombies, some are think quite good, but already can’t resist Death Mask influence. Their behavior is not very friendly toward Revil.
Places

Techno-Maze - structure that builds by electronic-magic symbiosis. Power of artifact makes electronic structure of maze to grow.
Techno-Clusters - towns that captured by Techno-Maze.
Towns - people living in neutral zone, places where techno-maze powers not very strong. Some of them are big megapolises, some of them are half-destroyed ghost-towns. Neutral zone is guarded by weapons. Techno-maze keep trying to get control of those towns (woo-hoo Matrix 3) Agents of Mask is living inside those towns, and trying to notice weak spots to help the Core to get control.
Villages - kinda real-life villages but in techno-maze context.

Level-Styles
Techno-Maze - generic Blame! electronic-architecture-style
Under-water - deep seas of techno-maze from inside
Planet surface - sand, grass’n stuff, forests, alien forests
Flame Steel Universe Aspects

In Flame Steel Universe you can be cyber-alien, or human-magician, or half-orc-elf accountant. So every creature has some unique abilities. But Revil - just ordinary boring human.
Races
Humans
Orcs
Elfs
Trolls
Halflings
Demons
Giants
Fairies (Angles (?))
Aliens (almost forgot about them dude!)

Creatures Abilities

Psy (mind reading, telekinesis etc, mind controlling etc)
Teleportation
Healing
Alien stuff
Something else?

Variations

Zombie
Vampire
Cyborg

Fractions

Fractions inside techno-maze is generating on game start. Some examples:
Half-elf-orc-cyborg-vampire fraction vs Psy-zombie-fairies fraction.
Bioshell Upgrades

Inside techno-maze Revil can find Bioshells. Those terminals allows to upgrade human flesh with implants (woo-hoo Deus Ex). You can make only 3 upgrades, so chose wisely.

Here list of bioshell upgrades goes
Special Encounters

Here is list of special encounters goes
